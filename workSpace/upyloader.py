# combines all files in a folder (where this script is located). featuring exceptions

import os
# from io import open

# set exceptions here
# EXCEPTIONS = ['upyloader.py', '.idea', 'main.py', '__init__.py', 'upload.bat']
# EXCEPTIONS = ['upyloader.py', 'main.py', '__init__']
OUTFILE_NAME = 'main.py'



print('Sstarting to combine Files...')
# get filenames
filenames = os.listdir()

if OUTFILE_NAME in filenames:
    # delete old file
    os.remove(OUTFILE_NAME)
    f = open(OUTFILE_NAME, "w+")
    f.close()

filenames.remove('upyloader.py')
filenames.remove('upyloader.bat')
filenames.remove('upyloader_howto.txt')
filenames.remove('__init__.py')
filenames.remove(OUTFILE_NAME)

# sort main.py file to bottom
filenames.remove('main_dev.py')
filenames.append('main_dev.py')

print('found valid files:')
print(filenames)


# combine files

lines = 0
with open(OUTFILE_NAME, 'w') as outfile:
    outfile.write('# Combined File: ################### to be uploaded to esp32 by upload script using ampy##########')
    for fname in filenames:
        with open(fname, 'r') as infile:
            for line in infile:
                outfile.write(line)
                lines += 1
            outfile.write('# end of file. ###########################################################################')

print("done combining to 'main.py'")
print(lines)
print('lines of code copied.')