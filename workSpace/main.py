# Combined File: ################### to be uploaded to esp32 by upload script using ampy########### all global imports here
import machine
from time import sleep
from time import ticks_us
from time import ticks_ms
from time import ticks_diff
# end of file. ############################################################################# start of file mymodule.net

def hsv_to_rgb(h, s, v):
    """
    Convert HSV to RGB (based on colorsys.py).

        Args:
            h (float): Hue 0 to 1.
            s (float): Saturation 0 to 1.
            v (float): Value 0 to 1 (Brightness).
    """
    if s == 0.0:
        return v, v, v
    i = int(h * 6.0)
    f = (h * 6.0) - i
    p = v * (1.0 - s)
    q = v * (1.0 - s * f)
    t = v * (1.0 - s * (1.0 - f))
    i = i % 6

    v = int(v * 255)
    t = int(t * 255)
    p = int(p * 255)
    q = int(q * 255)

    if i == 0:
        return v, t, p
    if i == 1:
        return q, v, p
    if i == 2:
        return p, v, t
    if i == 3:
        return p, q, v
    if i == 4:
        return t, p, v
    if i == 5:
        return v, p, q

## end of file# end of file. ############################################################################ Setup pins
pin2 = machine.Pin(2,machine.Pin.OUT)

# and write to them
pin2.value(1)
sleep(0.5)
pin2.value(0)
# end of file. ############################################################################ timer
# start_time = ticks_us()
# print("measuring the Time it takes to print this") # check time to print this
# sleep(0.1)
# delay = ticks_diff(ticks_us(), start_time) # calculate delay
# print(delay)
# print('in microseconds!')

# end of file. ############################################################################start of file main_dev.py - this is the main, but renamed to solve namespace problems

machine.freq(160000000)

print("starting")# end of file. ###########################################################################